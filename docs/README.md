# Prepackaged Cellpose for QiTissue

This is a package intended for installing Cellpose with QiTissue. The main purpose is to be able to install all dependencies easily and to provide an interface for QiTissue to use.

## Installation
This package can be installed using pip from the gitlab pypi registry
```bash
pip install prepacked-cellpose --index-url https://gitlab.com/api/v4/groups/claiv/-/packages/pypi/simple
```

## Testing
The package includes some tests that can be run to make sure the package is working properly on the  platform and environment.
```bash
pip install pytest
pytest --pyargs prepacked_cellpose
```
This will run some predictions on known data and compares them with the exprected outcome

## CLI
This package provides a cli that can be used via a CLI:

Example usage:
```bash
cellpose-cli -i path/to/input/file.npy -o file path/to/output/file.npy -m nuclei
```
Full list of options:

    -i, --image:           Input image path (required)
    -n, --image_nuclear:   Optional nuclear image path
    -m, --model_name:      The built-in pretrained model to use. Choices: ['cyto', 'nuclei', 'cyto2']. Default: 'cyto'
    -mf, --model_file:     Path to a file containing a Cellpose model
    -a, --net_avg:         Whether to average the networks. (Flag, no value required)
    -d, --diameter:        Average diameter of a cell in pixels. Float. Default: None == will try to determine it automatically (only for built-in models). For an external model passed via --model_file the diameter needs to be given
    -f, --flow_threshold:  Flow error threshold. Float. Default: 0.4
    -c, --cellprob_threshold: Cell probability threshold. Float. Default: 0.0
    -u, --augment:         Whether to augment the image. (Flag, no value required)
    -o, --output:          The path to the output file. Supports numpy array (.npy) and tiff file (.tiff/.tif) (required)
    -g, --gpu:             If GPU should be used. Defaults to 'auto'. Choices: ['auto', 'no']


To execute the Cellpose CLI, use the provided options to specify the input image, model, and other parameters. The output will be saved according to the specified output file format.

### Using a custom Model
Cellpose treats built-in models and user-provided models differently. The built-in models also provide a SizeModel that is used internally for preprocessing the images according to the cell sizes.

**Consequently, the diameter must be given if using a --model_file**

To pre-download the built-in models (including the SizeModel) download them into a folder externally, providing Cellpose with the path to the folder via the CELLPOSE_LOCAL_MODELS_PATH environment variable.

For example to download the cyto model it would be necessary to download and store the files
- https://www.cellpose.org/models/size_cytotorch_0.npy
- https://www.cellpose.org/models/cytotorch_0
- https://www.cellpose.org/models/cytotorch_1
- https://www.cellpose.org/models/cytotorch_2
- https://www.cellpose.org/models/cytotorch_3

and put them into CELLPOSE_LOCAL_MODELS_PATH

## Python
The package can also be used directly from python

### Instantiation
The package provides a Cellpose class that can be imported and instantiated:
```python
from prepacked_cellpose import Cellpose
algorithm = Cellpose('cyto', net_avg=False)
```
the contructor provides one parameter that lets the user choose the pretrained model to use out of 
`['cyto', 'nuceli', cyto2]` 

the default is `cyto`.
Cellpose will download the data for the model if they are not already downloaded.

`net_avg:` "loads the 4 built-in networks and averages them if True, loads one network if False" default is False. There are "4 different training versions, each trained starting from 4 different random initial parameter sets." 

(see [here](https://cellpose.readthedocs.io/en/latest/api.html) for more info)
### Prediction
A prediction can be made by calling predict on the instance

```python
result = algorithm.predict( np_array )
```

the function takes a numpy array and returns a numpy array. Specifically the signature is 

```python
def predict(self, image: np.ndarray[(np.uint16, np.uint16)], 
            image_nuclear: np.ndarray[(np.uint16, np.uint16)] = None, 
            diameter=None, flow_threshold=0.4, cellprob_threshold=0.0, augment=False) -> np.ndarray[(int, int)]:
```

**Parameters**: \
        image (np.ndarray): Input image to segment (2D grayscale image). \
        image_nuclear (np.ndarray, optional): An optional nuclear image of the same size. \
        diameter (float, optional): Average diameter of a cell in pixels. If None, the size is estimated on a per-image basis (only for built-in models). \
        flow_threshold (float, optional): Flow error threshold. Cells below this threshold are retained in the segmentation (default 0.4). \
        cellprob_threshold (float, optional): All pixels with a probability above this threshold are kept for masks (default 0.0). \
        augment (bool, optional): Whether to tile the image with overlapping tiles and flip overlapped regions to augment the segmentation (default False).

**Returns**:
    np.ndarray: The segmentation mask as a NumPy array. The shape of the array is (height, width).


(see [here](https://cellpose.readthedocs.io/en/latest/api.html) for more info)


