# [](https://gitlab.com/claiv/prepackaged-cellpose/compare/v3.1.2...v) (2023-10-30)



## [3.1.2](https://gitlab.com/claiv/prepackaged-cellpose/compare/v3.1.1...v3.1.2) (2023-10-30)


### Bug Fixes

* **cellpose:** enables cellpose gpu only for mac m1/2 due to error ([58d56c0](https://gitlab.com/claiv/prepackaged-cellpose/commit/58d56c0b7f0d7afe595f4d40aac8cf62523fd7af))
* **platform-check:** processor string check find is >= 0 ([1cb380e](https://gitlab.com/claiv/prepackaged-cellpose/commit/1cb380e14fedc94a7214b6d6b984ec4f365db38c))



## [3.1.1](https://gitlab.com/claiv/prepackaged-cellpose/compare/v3.1.0...v3.1.1) (2023-10-23)


### Bug Fixes

* **dependency:** remove explicit +cu118 as it is not available for all platforms ([0a1b7d7](https://gitlab.com/claiv/prepackaged-cellpose/commit/0a1b7d71eb24bc4b3e9c856f0dc9f864325c1bec))



# [3.1.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v3.0.0...v3.1.0) (2023-09-27)


### Bug Fixes

* **logging:** set default log level to info for cli ([847ef69](https://gitlab.com/claiv/prepackaged-cellpose/commit/847ef69b54acf06a6b05e8010478dace81a9a78c))
* **poetry:** added ctypes to dependencies ([06023d0](https://gitlab.com/claiv/prepackaged-cellpose/commit/06023d0a9f051e8cd6932a4fa0f3e1e7926c5694))


### Features

* **gpu:** add option to disable gpu acceleration, using it in tests because of different results ([2e58541](https://gitlab.com/claiv/prepackaged-cellpose/commit/2e58541f75e709953a55360eea1957fcf43b93c2))
* **logging:** some logging regarding the use of gpu acceleration ([49dee70](https://gitlab.com/claiv/prepackaged-cellpose/commit/49dee70054980e02daa1ca98d2c5f5cd38b3569a))



# [3.0.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v2.0.0...v3.0.0) (2023-08-17)


### Bug Fixes

* **gpu support:** fixed gpu activation for cellpose ([227625a](https://gitlab.com/claiv/prepackaged-cellpose/commit/227625aebf5261781e9ed550aa1e04022e38270e))
* **torch dependencies:** toml now requires torch 2.0.1, torchvision 0.15.2 and torchaudio 2.0.2 ([05415f3](https://gitlab.com/claiv/prepackaged-cellpose/commit/05415f3d7604bf38fb414f098847a19283d123c6))
* **typo:** Wrong method name prevented use of gpu for cellpose on Mac devices ([a7b67fb](https://gitlab.com/claiv/prepackaged-cellpose/commit/a7b67fb62890e5ff91f92b799bf1d82a8a2cbc93))


* feat!: add support for supplying custom model ([a0a3305](https://gitlab.com/claiv/prepackaged-cellpose/commit/a0a33053dfc5a99f441017d77380423323b25670))


### BREAKING CHANGES

* the cli and python api for supplying the model changed. It now takes either model_name for builtin pretrained models or model_file for supplying a model from disk



# [2.0.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.5.0...v2.0.0) (2023-07-14)


* refactor(cli)!: change cli name to avoid conflicts with cellpose ([90fa34a](https://gitlab.com/claiv/prepackaged-cellpose/commit/90fa34aa499d794b313a3073b377de041a5828f0))


### BREAKING CHANGES

* The cli is now called cellpose-cli instead of cellpose



# [1.5.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.4.0...v1.5.0) (2023-06-30)


### Bug Fixes

* **cli:** correct data type for diameter ([3c50360](https://gitlab.com/claiv/prepackaged-cellpose/commit/3c50360915dd88c0dc88ab469213b7b659daaadb))
* fix cuda check ([a04fd13](https://gitlab.com/claiv/prepackaged-cellpose/commit/a04fd134e5fb6a1cea6110cb4a75f2f4bad606fd))


### Features

* **progress_progress:** add progress dialog while installing and segmenting ([75affaf](https://gitlab.com/claiv/prepackaged-cellpose/commit/75affafb3331b1ea04fedc1f6cf0177fafc7fc7b))



# [1.4.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.3.4...v1.4.0) (2023-06-06)


### Features

* **cli:** implement cellpose cli like stardist with all parameters ([c8e19ab](https://gitlab.com/claiv/prepackaged-cellpose/commit/c8e19ab8a145cff9b83e261164bc96afaf2109cb))



## [1.3.4](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.3.3...v1.3.4) (2023-05-16)


### Bug Fixes

* remove cput dependency for now ([0d2ed56](https://gitlab.com/claiv/prepackaged-cellpose/commit/0d2ed56b5a870e67c91d6b6dd15e8b8816e2ca81))



## [1.3.3](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.3.2...v1.3.3) (2023-05-16)


### Bug Fixes

* **dependency:** explicit cpu ([e01bf4c](https://gitlab.com/claiv/prepackaged-cellpose/commit/e01bf4c169902e6d429f9947f82e926f70481008))



## [1.3.2](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.3.1...v1.3.2) (2023-05-16)


### Bug Fixes

* syntax error, add some docs ([c063972](https://gitlab.com/claiv/prepackaged-cellpose/commit/c063972b1846edbbeeb8ccf92181ca8ef60da427))



## [1.3.1](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.3.0...v1.3.1) (2023-05-15)


### Bug Fixes

* **semantic-release:** fix toml version variable ([0de3e1e](https://gitlab.com/claiv/prepackaged-cellpose/commit/0de3e1e965fdf023f9558a19d0ab43de859d17d6))



# [1.3.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.2.0...v1.3.0) (2023-05-15)


### Features

* optional nuclear channel in prediction ([a2a2fe9](https://gitlab.com/claiv/prepackaged-cellpose/commit/a2a2fe9bdf1bc897285ac85dc2d697d1a3f89fa7))



# [1.2.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.1.0...v1.2.0) (2023-05-08)


### Features

* **predict:** change default value of diameter to None ([a336d6d](https://gitlab.com/claiv/prepackaged-cellpose/commit/a336d6db0c7c292e3d3bd091dfafc34bce5bb709))



# [1.1.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v1.0.0...v1.1.0) (2023-05-08)


### Features

* **algorithm:** add net_avg, diameter, flow_threshold, cellprob_threshold, augment parameters ([0d3156c](https://gitlab.com/claiv/prepackaged-cellpose/commit/0d3156c9a01a1f5e43d3c1f48e34c6f0df21ba15))



# [1.0.0](https://gitlab.com/claiv/prepackaged-cellpose/compare/v0.0.1...v1.0.0) (2023-05-08)


### Features

* **algorithm:** add parameter to choose model in constructor ([4302eab](https://gitlab.com/claiv/prepackaged-cellpose/commit/4302eab684e8af085a04fc60d90eb40ec4211a5b))
* **cellpose:** add cellpose algorithm, dependencies ([1c94b65](https://gitlab.com/claiv/prepackaged-cellpose/commit/1c94b65b00524b82481a6100655a808cc7958b1e))


### BREAKING CHANGES

* **algorithm:** remove normalize option



## [0.0.1](https://gitlab.com/claiv/prepackaged-cellpose/compare/b62be32a135b2cdc91d01d64c583cc6273fc6348...v0.0.1) (2023-04-21)


### Bug Fixes

* **ci:** correct branch for semantic-release ([b62be32](https://gitlab.com/claiv/prepackaged-cellpose/commit/b62be32a135b2cdc91d01d64c583cc6273fc6348))



