import pytest
import numpy as np
from prepacked_cellpose import Cellpose
import importlib.resources
from . import data
from .fixtures.data import ex_prediction_input, ex_prediction_output

@pytest.mark.usefixtures('ex_prediction_input', 'ex_prediction_output')
def test_prediction_cyto(ex_prediction_input, ex_prediction_output):
    algorithm = Cellpose(model_name='cyto', use_gpu='no')

    labels = algorithm.predict(ex_prediction_input)

    assert np.array_equal(labels, ex_prediction_output)