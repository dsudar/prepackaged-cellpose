import pathlib
from pathlib import Path
from typing import ContextManager, Literal

import pytest
from .fixtures.data import ex_prediction_output, ex_input_data_path, ex_cytotorch0_30_prediction_output, ex_gpu_prediction_output
import subprocess
import numpy as np
import os
import tifffile as tiff
import logging
import requests


@pytest.mark.usefixtures('ex_prediction_output', 'ex_input_data_path')
def test_prediction_default(ex_prediction_output, ex_input_data_path):
    ex_output_data_path = './ex_output_data.npy'
    run_cellpose(ex_input_data_path, ex_output_data_path)
    
    labels = np.load(ex_output_data_path)

    assert np.array_equal(labels, ex_prediction_output)

    os.remove(ex_output_data_path)


@pytest.mark.usefixtures('ex_prediction_output', 'ex_input_data_path')
def test_prediction_default_tiff(ex_prediction_output, ex_input_data_path):
    ex_output_data_path = './ex_output_data.tiff'
    run_cellpose(ex_input_data_path, ex_output_data_path)

    labels = np.array(tiff.imread(ex_output_data_path))

    assert np.array_equal(labels, ex_prediction_output)

    os.remove(ex_output_data_path)


@pytest.mark.usefixtures('ex_gpu_prediction_output', 'ex_input_data_path')
def test_prediction_gpu(ex_gpu_prediction_output, ex_input_data_path):
    ex_output_data_path = './ex_output_data.npy'
    run_cellpose(ex_input_data_path, ex_output_data_path, use_gpu='auto')

    labels = np.load(ex_output_data_path)

    assert np.array_equal(labels, ex_gpu_prediction_output)

    os.remove(ex_output_data_path)


def run_cellpose(input_path: ContextManager[Path], output_path: str, extra_options: list[str] = [], use_gpu: Literal['auto', 'no'] = 'no'):
    with input_path as path:

        cmd = ['cellpose-cli', '-i', str(path.absolute()), '-o', output_path, '--gpu', use_gpu]
        cmd.extend(extra_options)

        result = subprocess.run(
            " ".join(cmd),
            capture_output=True, text=True, shell=True
        )
        logging.info(result.stdout)
        if result.stderr != '':
            logging.error(result.stderr)


@pytest.mark.usefixtures('ex_cytotorch0_30_prediction_output', 'ex_input_data_path')
def test_model_file(ex_cytotorch0_30_prediction_output, ex_input_data_path):

    model_name = 'cyto'

    # url_size = 'https://www.cellpose.org/models/size_cytotorch_0.npy'

    url = f'https://www.cellpose.org/models/{model_name}torch_0'
    logging.info(f'downloading model {url}')
    model_file = f'./{model_name}torch_0'
    response = requests.get(url)
    with open(model_file, 'wb') as file:
        file.write(response.content)

    model_file_path = pathlib.Path(model_file)
    ex_output_data_path = './ex_output_data.npy'
    run_cellpose(ex_input_data_path, ex_output_data_path, ['--model_file', str(model_file_path.absolute()), '--diameter', '30', '--gpu', 'no'])

    labels = np.load(ex_output_data_path)
    assert np.array_equal(labels, ex_cytotorch0_30_prediction_output)

    os.remove(model_file_path)
    os.remove(ex_output_data_path)
