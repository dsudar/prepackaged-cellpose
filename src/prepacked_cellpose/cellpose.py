import logging
import numpy as np
from typing import Literal
import torch
import platform


class Cellpose:
    def __init__(self, model_name: Literal['cyto', 'nuclei', 'cyto2'] = None, model_file: str = None, net_avg=False,
                 use_gpu: Literal['auto', 'no'] = 'auto'):
        """
        :param model_name: the builtin pretrained model to use
        :param model_file: the pretrained model file to use
        :param net_avg: "loads the 4 built-in networks and averages them if True, loads one network if False" default is False. There are "4 different training versions, each trained starting from 4 different random initial parameter sets."
        """
        from cellpose import models

        assert \
            model_name in ['cyto', 'nuclei', 'cyto2'] and model_file is None or \
            model_name is None and model_file is not None \
            , 'Supply either model_name or model_folder'

        logging.info('init Cellpose')
        torch_device = torch.device('cpu')
        gpu = False
        if use_gpu == 'auto':
            if platform.processor().find("arm") >= 0:
                if torch.backends.mps.is_available() and torch.backends.mps.is_built():
                    logging.info('mps backend available, using mps')
                    torch_device = torch.device('mps')
                    gpu = True
            elif torch.cuda.is_available():
                logging.info('cuda available, using cuda')
                torch_device = torch.device('cuda')
                gpu = True
            else:
                logging.info('using cpu')

        if model_name:
            self.model = models.Cellpose(gpu=gpu, model_type=model_name, net_avg=net_avg, device=torch_device)
        elif model_file:
            self.model = models.CellposeModel(gpu=gpu, pretrained_model=model_name, net_avg=net_avg,
                                              device=torch_device)

        self.net_avg = net_avg

    def predict(self, image: np.ndarray[(np.uint16, np.uint16)],
                image_nuclear: np.ndarray[(np.uint16, np.uint16)] = None, diameter=None, flow_threshold=0.4,
                cellprob_threshold=0.0, augment=False) -> np.ndarray[(int, int)]:
        """
        :param image: input image to segment (2d greyscale image)
        :param image_nuclear: an optional nuclear image of the same size
        :param diameter: average diameter of a cell in pixels, if None the size is estimated on a per-image basis (only for builtin models)
        :param flow_threshold: flow error threshold, cells below are kept (default 0.4)
        :param cellprob_threshold: all pixels above threshold probability are kept for masks (default 0.0)
        :param augment: tiles image with overlapping tiles and flips overlapped regions to augment
        :return: the prediction mask as numpy array
        """

        channels = [[0, 0]]
        input_image = image

        # channels = [cytoplasm, nucleus] (for cyto, cyto2 models!)
        # if NUCLEUS channel does not exist, set the second channel to 0
        # channels = [0,0]
        if image_nuclear is not None:
            input_image = np.stack([image, image_nuclear])
            channels = [[1, 2]]

        prediction = self.model.eval(
            input_image,
            channels=channels,  # grayscale image + no nuclear channel
            net_avg=self.net_avg,
            diameter=diameter,
            flow_threshold=flow_threshold,
            cellprob_threshold=cellprob_threshold,
            augment=augment,
            channel_axis=0
        )

        return prediction[0]
