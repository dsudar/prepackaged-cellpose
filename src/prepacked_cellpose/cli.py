import argparse
import logging

import numpy as np
import tifffile as tiff

from prepacked_cellpose import Cellpose


class CellposeCLI:
    def __init__(self):
        logging.getLogger().setLevel(logging.INFO)

        self.parser = argparse.ArgumentParser(description='Cellpose CLI')
        self.parser.add_argument('-i', '--image', help='input image path', required=True)
        self.parser.add_argument('-n', '--image_nuclear', help='optional nuclear image path')
        self.parser.add_argument('-m', '--model_name', help='the builtin pretrained model to use',
                                 choices=['cyto', 'nuclei', 'cyto2'])
        self.parser.add_argument('-mf', '--model_file', help='Path to a file containing a cellpose model')
        self.parser.add_argument('-a', '--net_avg', help='whether to average the networks',
                                 action='store_true')
        self.parser.add_argument('-d', '--diameter', help='average diameter of a cell in pixels, if not given will try to determine it automatically (only for built-in models). For an external model passed via --model_file the diameter needs to be given',
                                 type=float, default=None)
        self.parser.add_argument('-f', '--flow_threshold', help='flow error threshold',
                                 type=float, default=0.4)
        self.parser.add_argument('-c', '--cellprob_threshold', help='cell probability threshold',
                                 type=float, default=0.0)
        self.parser.add_argument('-u', '--augment', help='whether to augment the image',
                                 action='store_true')
        self.parser.add_argument('-o', '--output', help='the path to the output file. Supports numpy array (.npy) and tiff file (.tiff/.tif)', required=True)
        self.parser.add_argument('-g', '--gpu', help='if gpu should be used, defaults to auto', choices=['auto', 'no'], default='auto')

    def run(self):
        args = self.parser.parse_args()

        logging.info('Initializing Cellpose')
        model_name = args.model_name
        if model_name is None and args.model_file is None:
            model_name = 'cyto'
        cellpose = Cellpose(model_name=model_name, model_file=args.model_file, net_avg=args.net_avg, use_gpu=args.gpu)

        image = np.load(args.image)
        image_nuclear = None
        if args.image_nuclear:
            image_nuclear = np.load(args.image_nuclear)

        logging.info('Running Cellpose prediction')
        prediction = cellpose.predict(image=image, image_nuclear=image_nuclear,
                                      diameter=args.diameter, flow_threshold=args.flow_threshold,
                                      cellprob_threshold=args.cellprob_threshold, augment=args.augment)

        if args.output.endswith('.tif') or args.output.endswith('.tiff'):
            tiff.imsave(args.output, prediction.astype(np.uint16))
            logging.info(f'Prediction saved as {args.output} (TIFF format)')
        else:
            np.save(args.output, prediction)
            logging.info(f'Prediction saved as {args.output} (NumPy .npy format)')

def cli():
    CellposeCLI().run()